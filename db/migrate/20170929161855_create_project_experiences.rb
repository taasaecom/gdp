class CreateProjectExperiences < ActiveRecord::Migration[5.0]
  def change
    create_table :project_experiences do |t|
      t.references :user_profile, index: true, foreign_key: true
      t.string :duration
      t.string :title
      t.text :description
      t.text :result

      t.timestamps null: false
    end
  end
end
