class Api::Mobile::UserProfilesController::MentorSerializer < ActiveModel::Serializer
	cache
	attributes :id, :suggested_mentor, :approved, :updated_at

	has_one :user_profile, serializer: Api::Mobile::UserProfilesController::MentorProfileSerializer
	has_many :sent_messages
	has_many :received_messages

	def suggested_mentor
		object.id == 1
	end

	def approved
		mentor = UserMentor.where(user_id: @instance_options[:user_id], mentor_id: object.id).first
		if mentor
			mentor.approved
		else
			false
		end
	end
end