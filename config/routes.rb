Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: { registrations: 'users/registrations' } do 
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  resources :participants do
    member do
      put :approve
    end
    resources :chatrooms
  end

  get "/find_mentors" => 'mentors#find_mentors'
  resources :mentors do
    put 'add_mentor', param: :mentor_id
    resources :chatrooms
  end



  namespace :api do
    namespace :mobile do
      get 'user_profiles/options' => 'user_profiles#options'
      get 'user_profiles/mentors' => 'user_profiles#mentors'
      get 'user_profiles/participants_for_mentor' => 'user_profiles#participants_for_mentor'
      get 'user_profiles/available_mentors' => 'user_profiles#available_mentors'
      put 'user_profiles/update_mentor_list' => 'user_profiles#update_mentor_list'
      put 'user_profiles/update_participant_list' => 'user_profiles#update_participant_list'
      resources :user_profiles
      resources :users, only: [:update]
      devise_scope :user do
        post 'authenticate' => 'sessions#create', :as => 'login'
        delete 'authenticate' => 'sessions#destroy', :as => 'logout'
      end
      devise_for :users
      get 'messages/new_messages' => 'messages#new_messages'
      resources :messages
    end
  end
  namespace :admin_portal do
    resources :users do
      collection do
        get 'awaiting_access'
        get 'denied_access'
        get 'granted_access'
      end
      member do
        get 'badges'
        put 'grant_access'
        put 'deny_access'
        patch 'upload_new_profile_image'
      end
    end
  end
  get "home/index"
  get "home/minor"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: 'home#index'
end
