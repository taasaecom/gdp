class CreateUserProfileImages < ActiveRecord::Migration[5.0]
  def change
    create_table :user_profile_images do |t|
      t.references :user_profile, index: true, foreign_key: true
      t.references :image_type, index: true, foreign_key: true
      t.string :image_path

      t.timestamps null: false
    end
  end
end
