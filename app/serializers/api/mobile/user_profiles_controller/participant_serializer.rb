class Api::Mobile::UserProfilesController::ParticipantSerializer < ActiveModel::Serializer
	attributes :id, :approved, :updated_at

	has_one :user_profile, serializer: Api::Mobile::UserProfilesController::UserProfileSerializer

	def approved
		participant = UserMentor.where(mentor_id: @instance_options[:mentor_id], user_id: object.id).first
		if participant
			participant.approved
		else
			false
		end
	end
end