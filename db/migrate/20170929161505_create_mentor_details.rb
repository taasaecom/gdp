class CreateMentorDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :mentor_details do |t|
      t.references :user_profile, index: true, foreign_key: true
      t.integer :years_of_experience
      t.references :contribution_option, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
