class AddStateToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :access_state, :string
    add_column :users, :approved, :boolean, :default => false, :null => false
    add_index  :users, :approved
  end
end
