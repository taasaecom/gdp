class ParticipantsController < ApplicationController
	def index
		@participants = current_user.participants
		@participantsJSON = @participants.to_json
	end

	def show
		@participant = User.find(params[:id])
	end

	def approve
		@participant = User.find(params[:participant_id])
		@user_mentor = UserMentor.find_by(mentor_id: current_user.id, user_id: @participant.id)
		@user_mentor.approve
		redirect_to participants_path
	end
end
