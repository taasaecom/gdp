class Message < ApplicationRecord
  belongs_to :to_user, :class_name => 'User', :foreign_key => :to_user_id
  belongs_to :from_user, :class_name => 'User', :foreign_key => :from_user_id
  belongs_to :user

  after_create :enqueue_send_notification_job
  after_initialize :set_defaults, unless: :persisted?

  def enqueue_send_notification_job
    # Delayed::Job.enqueue(SendPushNotificationJob.new(self.id))
  	token = to_user.device&.uuid
  	notification = Houston::Notification.new(device: token)
  	notification.content_available = true
  	notification.badge = to_user.received_messages.where(read: false).count
    notification.sound = 'sosumi.aiff'
  	notification.alert = "#{from_user.first_name} #{from_user.last_name}: #{self.message}"
  	APN.push(notification)
  end

  def set_defaults
    self.read ||= false
  end

end
