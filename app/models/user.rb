class User < ApplicationRecord
  include TokenAuthenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_save :ensure_authentication_token

  validates :first_name, presence: true
  validates :last_name, presence: true
  # validates :title, presence: true

  has_one :user_profile, :dependent => :destroy
  accepts_nested_attributes_for :user_profile

  has_one :device, :dependent => :destroy
  accepts_nested_attributes_for :device
  
  after_create :add_user_profile

  after_validation :check_errors

  def check_errors
    Rails.logger.info "Errors: #{errors.full_messages}"
  end

  # has_many :mentors, :through => :user_mentors, :source => :user
  
  has_many :user_mentors_association, :class_name => "UserMentor", :foreign_key => :mentor_id
  has_many :participants, :through => :user_mentors_association, :source => :user
  has_many :inverse_user_mentors_association, :class_name => "UserMentor", :foreign_key => :user_id
  has_many :mentors, :through => :inverse_user_mentors_association, :source => :mentor

  has_many :messages
  has_many :sent_messages, :class_name => "Message", :foreign_key => :from_user_id
  has_many :received_messages, :class_name => "Message", :foreign_key => :to_user_id


  def self.admins
    admin_role = Role.find_by(:name => "Admin")
    User.joins(:user_profile).where(:user_profiles => { :role_id => admin_role.id })
  end

  def self.mentors
    mentor_role = Role.find_by(:name => "Mentor")
    User.joins(:user_profile).where(:user_profiles => { :role_id => mentor_role.id })
  end

  def self.participants
    participant_role = Role.find_by(:name => "Participant")
    User.joins(:user_profile).where(:user_profiles => { :role_id => participant_role.id })
  end


  def add_user_profile
    u = UserProfile.create! :user_id => id
    d = Device.create! :user_id => id, :uuid => SecureRandom.uuid
    save
  end

  def admin?
    admin_role = Role.find_by(name: 'Admin')
    user_profile&.role == admin_role
  end

  def participant?
    participant_role = Role.find_by(:name => "Participant")
    user_profile&.role == participant_role
  end

  def mentor?
    mentor_role = Role.find_by(:name => "Mentor")
    user_profile&.role == mentor_role
  end

  
  def active_for_authentication?
  	super && approved?
  end

  def inactive_message
  	if !approved?
  		:not_approved
  	else
  		super
  	end
  end

  def name
    return "#{self.first_name} #{self.last_name}"
  end

  def self.send_reset_password_instructions(attributes={})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.approved?
      recoverable.errors[:base] << I18n.t("devise.failure.not_approved")
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end

  after_create :send_admin_email
  def send_admin_email
    unless created_by_admin
      UserMailer.new_user_waiting_for_approval(self).deliver
    end
  end

  include AASM  
  aasm :access_state, :column => 'access_state' do
    state :awaiting_access, :initial => true
    state :granted_access
    state :denied_access

    event :grant_access do
      transitions :to => :granted_access, :after => Proc.new {|*args|
      	self.approved = true
        UserMailer.access_granted(self).deliver
      }
    end

    event :deny_access do
      transitions :to => :denied_access, :after => Proc.new {|*args|
      	self.approved = false
        UserMailer.access_denied(self).deliver
      }
    end
  end

  def available_mentors
    mentor_role_id = Role.find_by(name: "Mentor").id
    available_mentors = User.joins(:user_profile).where(:user_profiles => { :role_id => mentor_role_id})
    @mentors = available_mentors.where.not(id: self.mentor_ids)
  end

  def completion_rate
    total = 13.0
    count = 0
    count += user_profile.employee_number.nil? || user_profile.employee_number.blank? ? 0 : 1
    count += user_profile.job_title.nil? || user_profile.job_title.blank? ? 0 : 1
    count += user_profile.linkedin_profile.nil? || user_profile.linkedin_profile.blank? ? 0 : 1
    count += user_profile.bio.nil? || user_profile.bio.blank? ? 0 : 1
    count += user_profile.languages.nil? || user_profile.languages.blank? ? 0 : 1
    count += user_profile.institutions.nil? || user_profile.institutions.blank? ? 0 : 1
    count += user_profile.contribution_option_id.nil? ? 0 : 1
    count += user_profile.address&.address_1.nil? ? 0 : 1
    count += user_profile.mentor_detail&.years_of_experience.nil? || user_profile.mentor_detail&.years_of_experience.blank? ? 0 : 1
    count += user_profile.technical_expertises.count == 0 ? 0 : 1
    count += user_profile.interpersonal_supports.count == 0 ? 0 : 1
    count += user_profile.career_supports.count == 0 ? 0 : 1
    count += user_profile.user_profile_image&.image_path ? 0 : 1
    (count / total) * 100
  end


end













