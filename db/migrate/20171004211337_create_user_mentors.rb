class CreateUserMentors < ActiveRecord::Migration[5.0]
  def change
    create_table :user_mentors do |t|
      t.integer :user_id, :null => false
      t.integer :mentor_id, :null => false

      t.timestamps null: false
    end
  end
end
