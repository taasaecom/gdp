class Api::Mobile::UsersController < ApplicationController
	include ApiMobileConcerns


	def update
		if current_user.update_with_password(user_params)
			bypass_sign_in(current_user)
			render :status => 200
		end
	end

	private

	def user_params
		params.require(:user).permit(:current_password, :password, :password_confirmation)
	end
end
