class AddAttributesToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :bio, :text
  end
end
