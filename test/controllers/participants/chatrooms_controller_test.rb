require 'test_helper'

class Participants::ChatroomsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get participants_chatrooms_show_url
    assert_response :success
  end

end
