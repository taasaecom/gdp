class AddDescriptionToContributionOption < ActiveRecord::Migration[5.0]
  def change
    add_column :contribution_options, :description, :text
  end
end
