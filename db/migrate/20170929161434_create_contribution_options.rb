class CreateContributionOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :contribution_options do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
