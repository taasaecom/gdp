class AddContributionOptionToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_reference :user_profiles, :contribution_option, index: true, foreign_key: true
  end
end
