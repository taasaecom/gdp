class Api::Mobile::MessagesController < ApplicationController
	include ApiMobileConcerns
	
	def index
		sent = current_user.sent_messages.map { |m| Api::Mobile::MessagesController::MessageSerializer.new(m)}
		received = current_user.received_messages.map { |m| Api::Mobile::MessagesController::MessageSerializer.new(m)}
		messages = {:sent => sent, :received => received }
		render json: {:messages => messages }.to_json
	end

	def create
		@message = Message.new(message_params)
		if @message.save
			render json: @message, serializer: Api::Mobile::MessagesController::MessageSerializer
		else
			render :status => 401
		end
	end

	def update
		@message = Message.find(params[:id])
		if @message.update(message_params)
			render json: @message, serializer: Api::Mobile::MessagesController::MessageSerializer
		else
			render :status => 401
		end
	end 

	def new_messages
		unread_messages = current_user.received_messages.select { |m| !m.read }
		@messages = unread_messages.map { |m| Api::Mobile::MessagesController::MessageSerializer.new(m)}
		render json: {:messages => @messages }.to_json
	end


	private


	def message_params
		params.require(:message).permit(:id, :to_user_id, :from_user_id, :message, :read)
	end
end
