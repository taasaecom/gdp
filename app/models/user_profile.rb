class UserProfile < ApplicationRecord
  belongs_to :user
  accepts_nested_attributes_for :user
  
  belongs_to :region, optional: true
  belongs_to :business_line, optional: true
  belongs_to :institution, optional: true

  has_many :project_experiences, :dependent => :destroy
  accepts_nested_attributes_for :project_experiences
  
  has_one :address, :dependent => :destroy
  accepts_nested_attributes_for :address

  has_one :user_profile_image, :dependent => :destroy
  accepts_nested_attributes_for :user_profile_image

  belongs_to :contribution_option, optional: true

  has_and_belongs_to_many :technical_expertises
  has_and_belongs_to_many :interpersonal_supports
  has_and_belongs_to_many :career_supports
  has_one :mentor_detail, :dependent => :destroy
  accepts_nested_attributes_for :mentor_detail
  belongs_to :role, optional: true
  accepts_nested_attributes_for :role
  after_create :add_mentor_detail

  def add_mentor_detail
    mentor = MentorDetail.create! :user_profile_id => id
    save
  end

  def geocoded_state_location
    first = Geocoder.search("#{self.current_lat},#{self.current_long}").first
    first ? first.state : "Unknown"
  end


end
