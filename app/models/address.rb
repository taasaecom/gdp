class Address < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :address_type, optional: true
  belongs_to :user_profile, optional: true
end
