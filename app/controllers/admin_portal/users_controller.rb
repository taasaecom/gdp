class AdminPortal::UsersController < ApplicationController
  include RoleConcerns
  
  def new
    @user = User.new
    @user.build_user_profile
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "User has been created"
      redirect_to admin_portal_users_path(:id => @user.id)
    else
      Rails.logger.info @user.errors.full_messages
      message = "User has not been created"
      @user.errors.full_messages.each do |m|
        message += ", #{m}"
      end
      message += '.'
      flash[:error] = message
      render action: "new"
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def index
    @users = User.all
    @usersJson = User.all.to_json
  end

  def edit
    @user = User.find(params[:id])
    if !@user.user_profile.user_profile_image
      @user.user_profile.build_user_profile_image
    end
    if !current_user.admin? && (current_user != @user)
      flash[:error] = "You do not have access to that page"
      redirect_to root_path
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:success] = "User has been updated"
      redirect_to admin_portal_users_path(:id => @user.id)
    else
      flash[:error] = "User has not been updated, something went wrong"
      Rails.logger.info @user.errors.full_messages
      render action: "edit"
    end
  end

  def upload_new_profile_image
    @profile = User.find(params[:id]).user_profile
    if @profile.user_profile_image
      @profile.user_profile_image.update_attribute(:short_path, params[:short_path])
    else
      @image = UserProfileImage.create! :short_path => params[:image], :user_profile_id => @profile.id
    end
    @profile.save
  end

  def destroy
    @user.destroy
    flash[:success] = "User deleted"
    render action: 'index'
  end

  def awaiting_access
    @user = User.all
    @users = User.awaiting_access.page(params[:page])
  end
  
  def denied_access
    @user = User.all
    @users = User.denied_access.page(params[:page])
  end
  
  def granted_access
    @user = User.all
    @users = User.granted_access.page(params[:page])
  end
  
  def grant_access
    @user = User.find(params[:id])
    @user.grant_access!
    redirect_to admin_portal_users_path, notice: 'Access Granted'
  end
  
  def deny_access
    @user = User.find(params[:id])
    @user.deny_access!
    redirect_to admin_portal_users_path, alert: 'Access Denied'
  end

  private


  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :title, :password, :password_confirmation, :created_by_admin,
                                 user_profile_attributes: [:id, :contribution_option_id, :role_id, :job_title, :employee_number, :linkedin_profile, :bio, :languages, :institutions,
                                  technical_expertise_ids: [], interpersonal_support_ids: [], career_support_ids: [],
                                  address_attributes: [:id, :address_1, :address_2, :city, :state, :zip],
                                  user_profile_image_attributes: [:id, :short_path]])
  end
end
