class CreateBusinessLines < ActiveRecord::Migration[5.0]
  def change
    create_table :business_lines do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
