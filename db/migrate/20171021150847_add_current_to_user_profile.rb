class AddCurrentToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :current_city, :string
    add_column :user_profiles, :current_state, :string
    add_column :user_profiles, :current_country, :string
    add_column :user_profiles, :current_long, :float
    add_column :user_profiles, :current_lat, :float
  end
end
