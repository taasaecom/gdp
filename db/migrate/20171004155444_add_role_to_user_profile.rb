class AddRoleToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_reference :user_profiles, :role, index: true, foreign_key: true
  end
end
