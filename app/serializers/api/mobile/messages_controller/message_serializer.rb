class Api::Mobile::MessagesController::MessageSerializer < ActiveModel::Serializer
	attributes :id, :to_user_id, :from_user_id, :message, :from_user_profile_id, :created_at, :to_user_profile_id, :read

	def from_user_profile_id
		object.from_user&.user_profile&.id
	end

	def to_user_profile_id
		object.to_user&.user_profile&.id
	end
end