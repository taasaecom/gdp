class AddApprovedToUserMentor < ActiveRecord::Migration[5.0]
  def change
    add_column :user_mentors, :approved, :boolean
  end
end
