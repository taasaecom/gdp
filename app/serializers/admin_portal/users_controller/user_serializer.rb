class AdminPortal::UsersController::UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :access_state, :role, :completion_rate

  def completion_rate
    object.completion_rate
  end

  def role
    object.user_profile&.role.name
  end

end
