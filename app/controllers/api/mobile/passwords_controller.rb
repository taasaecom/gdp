class Api::Mobile::PasswordsController < Devise::RegistrationsController
	include ApiMobileConcerns
	respond_to :json
	protect_from_forgery with: :null_session
	before_action :resource_name

	def resource_name
		:user
	end

	def update
		super do
			render :status => 200,
           		:json => { :success => true,
                      :info => "Password Changed" } 
		end
	end

	private

	def after_update_path_for(resource)
		render :status => 200,
           :json => { :success => true,
                      :info => "Password Changed" }
	end

end