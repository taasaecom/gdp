class AddAuthTokenToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :authentication_token, :string
    add_index :users, :authentication_token, :unique => true
    User.all.each do |u|
      u.ensure_authentication_token
      u.save
    end
  end
end
