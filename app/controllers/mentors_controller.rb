class MentorsController < ApplicationController
	def index
		@mentors = current_user.mentors
		@mentorsJson = @mentors.to_json
	end

	def find_mentors
		@mentors = current_user.available_mentors
		@mentorsJson = @mentors.to_json
	end

	def show
		@mentor = User.find(params[:id])
	end

	def add_mentor
		@mentor = User.find(params[:mentor_id])
		current_user.mentors << @mentor
		redirect_to find_mentors_path
	end
end
