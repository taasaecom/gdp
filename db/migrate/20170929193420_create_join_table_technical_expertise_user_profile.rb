class CreateJoinTableTechnicalExpertiseUserProfile < ActiveRecord::Migration[5.0]
  def change
    create_join_table :technical_expertises, :user_profiles do |t|
      # t.index [:technical_expertise_id, :user_profile_id]
      # t.index [:user_profile_id, :technical_expertise_id]
    end
  end
end
