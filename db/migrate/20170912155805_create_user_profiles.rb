class CreateUserProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :user_profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.string :employee_number
      t.string :job_title
      t.references :region, index: true, foreign_key: true
      t.references :business_line, index: true, foreign_key: true
      t.date :hire_date
      t.references :institution, index: true, foreign_key: true
      t.string :linkedin_profile

      t.timestamps null: false
    end
  end
end
