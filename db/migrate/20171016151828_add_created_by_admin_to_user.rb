class AddCreatedByAdminToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :created_by_admin, :boolean
  end
end
