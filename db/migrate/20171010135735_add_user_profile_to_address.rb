class AddUserProfileToAddress < ActiveRecord::Migration[5.0]
  def change
    add_reference :addresses, :user_profile, index: true, foreign_key: true
  end
end
