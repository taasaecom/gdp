require "open-uri"

class UserProfileImage < ApplicationRecord
  belongs_to :user_profile
  belongs_to :image_type, optional: true

  mount_uploader :short_path, UserProfileImageUploader

  before_save :add_long_path, if: :short_path_changed?

  def add_long_path
  	path = "https://taasgdp.blob.core.windows.net/taasgdp/#{Rails.env}/profile_images/#{self.user_profile.id}/#{self.short_path.file.filename}";
  	self.image_path = path
  end

  def recreate_version!
  	if !image_path.nil?
  		self.remote_short_path_url = self.image_path
  		save
  	end
  end

end
