class SendPushNotificationJob < Struct.new(:message_id)
  def perform
    @message = Message.find(message_id)
	token = @message.to_user.device&.uuid
	notification = Houston::Notification.new(device: token)
	notification.content_available = true
	APN.push(notification)
  end
end
