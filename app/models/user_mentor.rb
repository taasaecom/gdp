class UserMentor < ApplicationRecord
  belongs_to :user, :class_name => "User"
  belongs_to :mentor, :class_name => "User"

  after_create :set_approved

  def set_approved
  	update_attribute(:approved, false)
  	save
  end

  def approve
  	update_attribute(:approved, true)
  	save
  end
end
