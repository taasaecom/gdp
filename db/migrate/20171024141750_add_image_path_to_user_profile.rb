class AddImagePathToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :image_path, :string
  end
end
