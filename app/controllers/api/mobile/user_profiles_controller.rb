class Api::Mobile::UserProfilesController < ApplicationController
	include ApiMobileConcerns

	def show
		@user_profile = current_user.user_profile
		render json: @user_profile, serializer: UserProfileSerializer, is_current_user: true, root: false
	end

	def update
		@user_profile = current_user.user_profile
		if @user_profile.update(user_profile_params)
			render json: @user_profile, serializer: UserProfileSerializer, root: false
		else
			render json: {:error => true}
		end
	end

	def options
		@career_supports = CareerSupport.all.map { |c| { id: c.id, name: c.name, updated_at: c.updated_at } }
		@interpersonal_supports = InterpersonalSupport.all.map { |c| { id: c.id, name: c.name, updated_at: c.updated_at } }
		@technical_expertises = TechnicalExpertise.all.map { |c| { id: c.id, name: c.name, updated_at: c.updated_at } }
		@contribution_options = ContributionOption.all.map { |c| { id: c.id, name: c.name, description: c.description, updated_at: c.updated_at } }
		render json: { career_supports: @career_supports, interpersonal_supports: @interpersonal_supports, technical_expertises: @technical_expertises, contribution_options: @contribution_options }.to_json
	end

	def available_mentors
		mentor_role_id = Role.find_by(name: "Mentor").id
		available_mentors = User.joins(:user_profile).where(:user_profiles => { :role_id => mentor_role_id})
		@mentors = available_mentors.where.not(id: current_user.mentor_ids)
		# mentorJSON = @mentors.map { |m| MentorSerializer.new(m) }
		render json: @mentors, each_serializer: MentorSerializer, root: "mentors", adapter: :json
	end

	def mentors
		render json: current_user.mentors, each_serializer: MentorSerializer, user_id: current_user.id, root: "mentors", adapter: :json
	end

	def update_mentor_list
		current_user.mentors << User.find(params[:mentor_id])
		current_user.save
		if current_user.errors
			render json: {:error => true}.to_json
		else
			render :status => 200
		end
	end

	def update_participant_list
		participant = current_user.user_mentors_association.select { |p| p.user_id == params[:participant_id] }
		participant[0].update_attribute(:approved, true)
		render json: {:errors => participant[0].errors }
	end


	def participants_for_mentor
		render json: current_user.participants, each_serializer: ParticipantSerializer, mentor_id: current_user.id, root: "participants", adapter: :json
	end


	def user_profile_params
		params.require(:user_profile).permit(:id, :job_title, :languages, :linkedin_profile, :bio, :institutions,
											 :current_city, :current_state, :current_country, :current_lat, :current_long, :contribution_option_id,
											 :technical_expertise_ids => [], :career_support_ids => [], :interpersonal_support_ids => [],
									  		 mentor_detail_attributes: [:id, :years_of_experience],
									  		 user_profile_image_attributes: [:id, :image_path],
									  		 project_experiences_attributes: [:id, :duration, :title, :description, :result],
									  		 address_attributes: [:id, :address_1, :address_2, :city, :state, :zip],
									  		 user_attributes: [:id, :first_name, :last_name,
									  		 			device_attributes: [:id, :uuid]]
									  		 )
	end
end
