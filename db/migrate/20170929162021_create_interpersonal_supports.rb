class CreateInterpersonalSupports < ActiveRecord::Migration[5.0]
  def change
    create_table :interpersonal_supports do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
