require 'test_helper'

class Mentors::ChatroomsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get mentors_chatrooms_show_url
    assert_response :success
  end

end
