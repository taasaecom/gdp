class UserMailer < ApplicationMailer
	default from: 'no-reply@aecomonline.net'

	def new_user_waiting_for_approval(user)
    @user = user
    mail(
      :to => User.admins.map {|u| "#{u.name} <#{u.email}>"}.join(','),
      :subject => "[GDP] New Access Request"
    )
  end
  
  def access_granted(user)
    @user = user
    mail(
      :to => "#{@user.name} <#{@user.email}>",
      :subject => "[GDP] Access Request Granted"
    )
  end
  
  def access_denied(user)
    @user = user
    mail(
      :to => "#{@user.name} <#{@user.email}>",
      :subject => "[GDP] Access Request Denied"
    )
  end
  
  def account_created(user)
    @user = user
    mail(
      :to => "#{@user.name} <#{@user.email}>",
      :subject => "[GDP] Account Created"
    )
  end
end
