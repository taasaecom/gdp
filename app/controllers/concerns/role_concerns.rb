module RoleConcerns
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_role, only: [:index]
  end

  protected

  def authenticate_role
    admin_role = Role.find_by(:name => 'Admin')
    if current_user.user_profile&.role != admin_role
      flash[:error] = "You do not have permission to access this page"
      redirect_to edit_user_registration_path(current_user)
    end
  end


end