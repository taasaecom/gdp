source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
gem 'puma', '~> 3.7'
gem 'bootstrap-sass', '~> 3.3.7'
gem 'font-awesome-rails'

# Database gem
gem 'mysql2', '>= 0.3.18', '< 0.5'
gem 'jquery-rails'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.2'

# State management
gem 'aasm', '~> 4.3.0'

# mailing API
gem 'sendgrid'

# JSON Serializer
gem "active_model_serializers"

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Sign-In/Authentication Gem
gem 'devise'
gem 'simple_token_authentication'

# background job gem
gem 'delayed_job_active_record'
gem "daemons"

# APSN push notification ge
gem 'houston'

# Azure Storage
gem "azure"

# Image uploader with azure
gem 'carrierwave-azure_rm'
gem "mini_magick"

# Geo Location
gem 'geocoder'

# Dropzone for file upload
gem 'rails-assets-dropzone', source: 'https://rails-assets.org'

gem 'listen', '>= 3.0.5', '< 3.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
	gem 'pry-rails', '~> 0.3.4'
	gem "better_errors"
	gem 'capistrano'
	gem 'capistrano-bundler'
	gem 'capistrano-rails'
	gem 'capistrano-rvm', github: "capistrano/rvm"
	gem 'web-console', '>= 3.3.0'
  	# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
	gem 'spring'
	gem 'spring-watcher-listen', '~> 2.0.0'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
