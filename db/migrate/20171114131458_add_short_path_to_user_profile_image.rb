class AddShortPathToUserProfileImage < ActiveRecord::Migration[5.1]
  def change
    add_column :user_profile_images, :short_path, :string
  end
end
