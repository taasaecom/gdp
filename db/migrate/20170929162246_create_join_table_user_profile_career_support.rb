class CreateJoinTableUserProfileCareerSupport < ActiveRecord::Migration[5.0]
  def change
    create_join_table :user_profiles, :career_supports do |t|
      # t.index [:user_profile_id, :career_support_id]
      # t.index [:career_support_id, :user_profile_id]
    end
  end
end
