class MentorDetail < ApplicationRecord
  belongs_to :user_profile
  belongs_to :contribution_option, optional: true
end
