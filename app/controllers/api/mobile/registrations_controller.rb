class Api::Mobile::RegistrationsController < Devise::RegistrationsController
	respond_to :json
	protect_from_forgery with: :null_session
	before_action :configure_sign_up_params, only: [:create]
	before_action :configure_update_params, only: [:update]
	before_action :resource_name

	def resource_name
		:user
	end

	def create
		super do
			if resource.errors.full_messages.length > 0
				render :status => 200,
	           		:json => { :success => false,
	                      :info => resource.errors.full_messages
	                     } and return
			else
				render :status => 200,
		           :json => { :success => true,
		                      :info => "Created Account"
		                     } and return
		    end
	    end 
	end

	def update
		super do
			if resource.errors.full_messages.length > 0
				render :status => 200,
	           		:json => { :success => false,
	                      :info => resource.errors.full_messages
	                     } and return
			else
				render :status => 200,
		           :json => { :success => true,
		                      :info => "Account Updated"
		                     } and return
		    end
	    end 
	end

	private

	def after_sign_up_path_for(resource)
		render :status => 200,
           :json => { :success => true,
                      :info => "Created Account" }
	end

	def configure_sign_up_params
		devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :title, :email, :password, :password_confirmation])
	end

	def configure_update_params
		devise_parameter_sanitizer.permit(:update_account, keys: [:email, :password, :password_confirmation, :current_pasword])
	end
end