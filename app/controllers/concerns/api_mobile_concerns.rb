module ApiMobileConcerns
  extend ActiveSupport::Concern
  included do
    respond_to :json
    protect_from_forgery with: :null_session
    before_action :authenticate_user_with_token!
    before_action :authenticate_user!
  end

  protected

  def authenticate_user_with_token!
    user_token = request.headers['Auth-Token']
    user = user_token && User.find_by_authentication_token(user_token)
    if user
      # Notice we are passing store false, so thte user is not
      # actually store in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in sotken, you can simply remove store false
      sign_in user, store: false
    end
  end
end