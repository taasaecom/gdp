class Api::Mobile::SessionsController < Devise::SessionsController
  include ApiMobileConcerns
  skip_before_action :verify_signed_out_user

  # curl -H "Content-Type: application/json"
  #   -H 'Accept: application/json'  
  #   -X POST http://localhost:5000/api/mobile/authenticate
  #   -d '{"user": {"email":"nathan@brandnewbox.com","password":"password"}}' 
  def create
    warden.authenticate!(:scope => resource_name, :store => false, :recall => "#{controller_path}#failure")
    token = current_user.authentication_token
    id = current_user.id
    # The following configuration in devise.rb:80 only skips session storage when using token authentication:
    # # config.skip_session_storage = [:http_auth, :token_auth] 
    # Since we are not doing that here, a session is created. We don't want this.
    # So we forcibly log out here so that the session gets cleared.
    warden.logout
    render :status => 200,
           :json => { :success => true,
                      :info => "Logged in",
                      :data => { 
                        :auth_token => token,
                        :id => id
                       }
                     }
  end

  # curl -X DELETE -H "Auth-Token: k3nlfknsnefeqlkfn" -H "Content-Type: application/json" http://localhost:5000/api/mobile/authenticate
  def destroy
    # We don't need this because the user is authenticated by the token.
    # warden.authenticate!(:scope => resource_name, :store => false, :recall => "#{controller_path}#failure")
    if current_user
      current_user.reset_authentication_token! 
      render :status => 200,
             :json => { :success => true,
                        :info => "Logged out",
                        :data => {} }
    else
      render :status => 200,
             :json => { :success => true,
                        :info => "Couldn't find user",
                        :data => {} }
    end
  end
  
  def failure
    render :status => 401,
           :json => { :success => false,
                      :info => "Login Failed",
                      :data => {} }
  end

  # Other code
end