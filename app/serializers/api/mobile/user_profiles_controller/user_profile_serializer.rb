class Api::Mobile::UserProfilesController::UserProfileSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :job_title, :linkedin_profile, :bio, :languages, :updated_at,
             :years_of_experience, :role, :email, :name, :mentor_ids,
             :interpersonal_support_ids, :career_support_ids, :technical_expertise_ids,
             :mentors, :project_experiences, :is_current_user, :last_name, :first_name, :device_id,
             :current_city, :current_state, :current_country, :current_lat, :current_long, :institutions, :contribution_option_id

  has_one :mentor_detail
  has_one :address
  has_one :user_profile_image

  def is_current_user
    if @instance_options[:is_current_user]
      @instance_options[:is_current_user]
    else
      false
    end
  end

  def interpersonal_support_ids
    object.interpersonal_support_ids
  end

  def career_support_ids
    object.career_support_ids
  end

  def technical_expertise_ids
    object.technical_expertise_ids
  end

  def years_of_experience
  	object.mentor_detail&.years_of_experience
  end

  def role
  	object.role.name
  end

  def name
  	object.user.name
  end

  def project_experiences
    object.project_experiences
  end

  def email
  	object.user.email
  end

  def mentor_ids
  	object.user.mentor_ids
  end

  def mentors
    object.user.mentors.map {|mentor| Api::Mobile::UserProfilesController::MentorSerializer.new(mentor)}
  end

  def first_name
    object.user.first_name
  end

  def last_name
    object.user.last_name
  end

  def device_id
    object.user.device.id
  end

end
