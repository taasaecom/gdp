class AddInstitutionsToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :institutions, :string
  end
end
