class CreateJoinTableUserProfileInterpersonalSupport < ActiveRecord::Migration[5.0]
  def change
    create_join_table :user_profiles, :interpersonal_supports do |t|
      # t.index [:user_profile_id, :interpersonal_support_id]
      # t.index [:interpersonal_support_id, :user_profile_id]
    end
  end
end
