class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.references :to_user, index: true
      t.references :from_user, index: true
      t.text :message
      t.boolean :read

      t.timestamps null: false
    end
  end
end
